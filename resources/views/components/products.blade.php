<section class="portfolio">
        <div class="container">
          <div class="section-wrap">
              <div class="row clearfix">
                <div  class="col-md-3 fix-padding w-30 fix-category-movil" >
                    <article class="post item  imagePanelInfo animated fast disable-movil">
                        <div class="entry-thumbnail">
                            <img id="imageInfo" class="img-responsive" src="{{ asset ('img/products/infopremium.jpg') }}" alt="">
                            <div class="post-meta">
                                <div class="big-title  animated delay-2s rubberBand">
                                        <img id="logoInfo" src="{{ asset('img/colors/premium.jpg') }}" alt="">
                                    <div class="divider-2"></div>
                                    <p id="textInfo" class="jsregular-22">Concentrados elaborados con ingredientes 100% naturales que nos permite ofrecer a nuestros clientes una opción con una consistencia, aroma y sabor que realza la experiencia de las auténticas aguas frescas.<br> <br>Además busca satisfacer una diversidad de usos con  un mismo producto; nuestros perfiles y rendimiento hace que Porto Alegre Premium sea ideal para la elaboración de bebidas de autor y apoyo esencial para chefs y mixólogos.</p>
                                    <br>
                                    <div class="divider-2"></div>
                                </div>
                            </div>
                        </div> 
                    </article>
                    <article class="post item  imagePanelInfo animated fast active-movil">
                        <div class="entry-thumbnail">
                            <div class="post-meta">
                                <div class="big-title  animated delay-2s rubberBand">
                                        <img class="img-responsive" id="logoMovil" src="{{ asset ('/img/products/premium_0.jpg') }}" alt="">
                                    <div class="divider-2"></div>
                                    <p id="textInfo" class="jsregular-22">Concentrados elaborados con ingredientes 100% naturales que nos permite ofrecer a nuestros clientes una opción con una consistencia, aroma y sabor que realza la experiencia de las auténticas aguas frescas.<br> <br>Además busca satisfacer una diversidad de usos con  un mismo producto; nuestros perfiles y rendimiento hace que Porto Alegre Premium sea ideal para la elaboración de bebidas de autor y apoyo esencial para chefs y mixólogos.</p>
                                    <br>
                                    <div class="divider-2"></div>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="portfolio col-md-9 gutter-less mtn w-70" >
                    <div class="row">
                        <div data-category="premium" class="portfolio-item default-gallery-item style_2 item-one col-lg-4 col-md-6 col-sm-4 col-xs-4 categoryContent" style="display:none;">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box category" data-category="premium" >
                                    <img src="{{ asset ('img/products/premium.jpg') }}"  class="img-responsive">
                                    <a href="javascript:;" class="lightbox-image image-link"><span class="icofont icofont-plus-circle"></span></a>
                                
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                        <div data-category="withoutsugar" class="portfolio-item default-gallery-item style_2 item-one col-lg-4 col-md-6 col-sm-4 col-xs-4 categoryContent" style="max-height: 300px;">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box category" data-category="withoutsugar">
                                    <img src="{{ asset ('img/products/withoutSuggar.jpg') }}"  class="img-responsive">
                                    <a href="javascript:;" class="lightbox-image image-link"><span class="icofont icofont-plus-circle"></span></a>
                                
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                        <div data-category="bbplus" class="portfolio-item default-gallery-item style_2 item-two col-lg-4 col-md-6 col-sm-4 col-xs-4 categoryContent" style="max-height: 300px;">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <figure class="image-box category" data-category="bbplus">
                                <img src="{{ asset ('img/products/bbplus.jpg') }}" alt="" class="img-responsive">
                                <a href="javascript:;" class="lightbox-image image-link"><span class="icofont icofont-plus-circle"></span></a>
                            </figure>
                            <!--Overlay Box-->
                        </div>                          
                        </div>
                        <div data-category="readydrink" class="portfolio-item default-gallery-item style_2 item-one item-three col-lg-4 col-md-6 col-sm-4 col-xs-4 categoryContent" style="max-height: 300px;">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <figure class="image-box category" data-category="readydrink">
                                <img src="{{ asset ('img/products/readyStart.jpg') }}" alt="" class="img-responsive">
                                <a href="javascript:;" class="lightbox-image image-link" ><span class="icofont icofont-plus-circle"></span></a>
                            </figure>
                            <!--Overlay Box-->
                        </div>                          
                        </div>
                    </div>
                    <div class="row">
                        <div  class="portfolio-item default-gallery-item style_2 item-one col-md-12">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box">
                                    <a class="contact-btn" href="/contact/#contactForm">QUEREMOS HACER NEGOCIOS CONTIGO</a>
                                    <img class="img-flavor animated fadeInUp" src="{{ asset ('img/products/premium/premium0.jpg') }}" alt="" class="img-responsive" >
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 fix-padding">
                            <div class="premium-slider flavors" id="premium" > 
                                <!-- Logo item -->
                                <div data-flavor="premium0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor0.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor1.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor2.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium3.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor3.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium4.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor4.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium5.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor5.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium6.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor6.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium8.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor11.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium7.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor7.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-12 fix-padding">
                        <div style="display:none;" class="withoutSugar-slider flavors" id="withoutsugar"> 
                            <!-- Logo item -->
                            <div data-flavor="withoutsugar0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="withoutsugar1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="withoutsugar2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="withoutsugar3.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor3.jpg')}}"></div>
                            <div data-flavor="withoutsugar4.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor4.jpg')}}"></div>
                            <div data-flavor="withoutsugar5.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor5.jpg')}}"></div>
                            <div data-flavor="withoutsugar6.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor6.jpg')}}"></div>
                            <div data-flavor="withoutsugar7.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor7.jpg')}}"></div>
                            <div data-flavor="withoutsugar8.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor8.jpg')}}"></div>
                            <div data-flavor="withoutsugar9.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor9.jpg')}}"></div>
                            <div data-flavor="withoutsugar10.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor10.jpg')}}"></div>
                            <div data-flavor="withoutsugar11.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor11.jpg')}}"></div>
                            <div data-flavor="withoutsugar12.jpg" class="item flavor"><img alt="" src="{{asset('img/products/withoutsugar/flavors/flavor12.jpg')}}"></div>

                        </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-12 fix-padding">
                        <div style="display:none;" class="bbplus-slider flavors" id="bbplus"> 
                            <!-- Logo item -->
                            <div data-flavor="bbplus0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="bbplus1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="bbplus2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="bbplus3.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor3.jpg')}}"></div>
                            <div data-flavor="bbplus4.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor4.jpg')}}"></div>
                            <div data-flavor="bbplus5.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor5.jpg')}}"></div>
                            <div data-flavor="bbplus6.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor6.jpg')}}"></div>
                            <div data-flavor="bbplus7.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor7.jpg')}}"></div>
                            <div data-flavor="bbplus8.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor8.jpg')}}"></div>
                            <div data-flavor="bbplus9.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor9.jpg')}}"></div>
                            <div data-flavor="bbplus10.jpg" class="item flavor"><img alt="" src="{{asset('img/products/bbplus/flavors/flavor10.jpg')}}"></div>

                        </div>
                        </div>
                    </div>
                    <div class="row " >
                        <div class="col-md-12 fix-padding">
                        <div style="display:none;" class="readyDrink-slider flavors" id="readydrink"> 
                            <!-- Logo item -->
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="readydrink0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="readydrink0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                        </div>
                        </div>
                    </div>
                </div>
                  
              </div>
          </div>
       </div>
    </section>



@section('scripts')

<script>
    var selected='premium';
    var currentImg= '';
    var text = {
        premium : 'Concentrados elaborados con ingredientes 100% de origen natural que nos permite ofrecer a nuestros clientes una opción con una consistencia, aroma y sabor que realza la experiencia de las auténticas AGUAS FRESCAS. Además busca satisfacer una diversidad de usos con un mismo producto; nuestros perfiles y rendimiento hace que Porto Alegre Premium sea ideal para la elaboración de  bebidas con fruta, de autor y apoyo esencial para chefs y mixólogos.',
        bbplus : 'Concentrados para la elaboración de AGUAS FRESCAS dirigidos al canal de ALTO CONSUMO. Nuestros COMEDORES INDUSTRIALES logran ofrecer una bebida con sabor, Su gran variedad de opciones permite a los consumidores tener amplias y diversas opciones de bebidas en su centro de consumo.',
        withoutsugar : 'Concentrados elaborados SIN AZÚCAR pero manteniendo el mismo sabor a fruta. Nuestras bebidas pensadas fueron realizadas para contribuir al bienestar de nuestros clientes y consumidores. Su aroma y sabor siguen deleitando los paladares de quienes buscan una bebida SIN CALORÍAS.',
        readydrink :'SOLO AGITA ABRE Y DISFRUTA. \n Las Auténticas AGUAS FRESCAS disponibles siempre listas y siempre frescas para disfrutarlas con amigos y en familia. ¡PORTO ALEGRE bebidas son para toda ocasión! Encuéntranos en los principales autoservicios. * \n\n *Disponible sólo en Hermosillo,Sonora.'
    }
    
    $('.flavor').click(function(){
        $('.img-flavor').removeClass('fadeInUp');
        $('.img-flavor').addClass('fadeOutUp');
        currentImg = $(this).attr('data-flavor');
        setTimeout(function() {   //calls click event after a certain time
            $('.img-flavor').attr('src','/img/products/'+selected+'/'+currentImg);
            $('.img-flavor').removeClass('fadeOutUp');
            $('.img-flavor').addClass('fadeInUp');
         }, 300);
    });

    $('.category').click(function(){
        selected = $(this).attr('data-category');
        $('.img-flavor').attr('src','/img/products/'+selected+'/'+selected+'0.jpg');

        $('.flavors').css('display','none');
        $('#'+$(this).attr('data-category')).css('display','block');
        $('#textInfo').text(text[selected]);
        animateCSS('.imagePanelInfo', 'bounceOutLeft',function() {
            $('#imageInfo').attr('src','/img/products/'+'info'+selected+'.jpg');
            animateCSS('.imagePanelInfo', 'bounceInLeft');
        });
        $('#logoMovil').attr('src','/img/products/'+selected+'_0.jpg');
        $.each($('.categoryContent'), function( key, item ) {
            if($(item).attr('data-category') == selected){
                $(item).css('display','none');
            }else{
                $(item).css('display','block');
            }
        });
    });

    function animateCSS(element, animationName, callback) {
        const node = document.querySelector(element)
        node.classList.add('animated', animationName)
    
        function handleAnimationEnd() {
            node.classList.remove('animated', animationName)
            node.removeEventListener('animationend', handleAnimationEnd)
    
            if (typeof callback === 'function') callback()
        }
    
        node.addEventListener('animationend', handleAnimationEnd)
    }
   

</script>

@endsection