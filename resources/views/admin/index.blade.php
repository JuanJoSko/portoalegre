@extends('layouts.admin')

@section('content')
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    
    <div class="app-main">
        <div class="app-sidebar sidebar-shadow bg-asteroid sidebar-text-light">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="scrollbar-sidebar">
                <div class="app-sidebar__inner">
                    <ul class="vertical-nav-menu">
                        <li class="app-sidebar__heading">Dashboard</li>
                        <li>
                            <a href="index.html" class="mm-active">
                                <i class="metismenu-icon pe-7s-graph2"></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="app-sidebar__heading">Inicio</li>
                        <li>
                            <a href="dashboard-boxes.html" aria-expanded="false">
                                <i class="metismenu-icon pe-7s-photo"></i>
                                Banners
                            </a>
                        </li>
                        <li>
                            <a href="tables-regular.html">
                                <i class="metismenu-icon pe-7s-news-paper"></i>
                                Textos
                            </a>
                        </li>
                        <li class="app-sidebar__heading">Productos</li>
                        <li>
                            <a href="dashboard-boxes.html" aria-expanded="false">
                                <i class="metismenu-icon pe-7s-photo"></i>
                                Banners
                            </a>
                        </li>
                        <li>
                            <a href="tables-regular.html">
                                <i class="metismenu-icon pe-7s-news-paper"></i>
                                Textos
                            </a>
                        </li>
                        <li class="app-sidebar__heading">Encuentranos</li>
                        <li>
                            <a href="dashboard-boxes.html" aria-expanded="false">
                                <i class="metismenu-icon pe-7s-photo"></i>
                                Banners
                            </a>
                        </li>
                        <li>
                            <a href="tables-regular.html">
                                <i class="metismenu-icon pe-7s-news-paper"></i>
                                Textos
                            </a>
                        </li>
                        <li class="app-sidebar__heading">USA</li>
                        <li>
                            <a href="dashboard-boxes.html" aria-expanded="false">
                                <i class="metismenu-icon pe-7s-photo"></i>
                                Banners
                            </a>
                        </li>
                        <li>
                            <a href="tables-regular.html">
                                <i class="metismenu-icon pe-7s-news-paper"></i>
                                Textos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-graph2 icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>PortoAlegre Dashboard
                                <div class="page-title-subheading">dashboard
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <img style="margin: auto;
                    margin-top: 10%;" src="{{ asset('/img/logo.png') }}" alt="">
                </div>
            </div>
            
        </div>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    </div>
</div>


@endsection