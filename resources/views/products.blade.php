@extends('layouts.app')

@section('content')

    <main class="">
        <!-- Header -->
        <div
            style="background-image: url('{{asset('/img/products/banner01.png')}}'); width:100% ;background-size: cover;background-repeat: no-repeat;">
                <div class="row" >
                    <div class="col-sm-6 col-xs-12 m-b-4 ml-60 banner-text">
                        <div class="product-title  animated delay-1s flipInY">
                            <h1>CATÁLOGO<br><span>DE PRODUCTOS</span></h1>
                            <div class="banner-description disable-movil">
                                <p>Las Auténticas AGUAS FRESCAS son bebidas milenarias que intensifican los sabores naturales de nuestras tradiciones más mexicanas. </p>
                                <br>
                                <p>Nuestros concentrados y bebidas ofrecen una experiencia  PRÁCTICA, SENCILLA Y VERSÁTIL, garantizando CALIDAD y DIVERSIDAD en sus sabores y usos.</p>
                                <br>
                                <p>Encuentra en NUESTRO CATÁLOGO las distintas EXPERIENCIAS de SABOR que tenemos para ti, tu negocio y familia.</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </main>
    <section class="testimonial style_2 mt60 active-movil">
        <div class="section-content">
        <div class="row">
            <div class="container">
                <div class="col-md-6 about-text text-center">
                    <p class="blue-color jsregular-18">Nuestros productos ha sido creados por y para nuestros clientes de Restaurantes, Comedores , Cafeterías y Hoteles.</p>
                    <p class="blue-color jsregular-18">Fácil, Práctico, Versátil y Rentable son los principales atributos que ofrecemos en cada uno de nuestros sabores disponibles.</p>
                </div>
            </div>
            
        </div>
    </div>
  </section>


    @include('components.products')

    {{--  <section class="portfolio disable-movil">
        <div class="col-md-12">
            <div class="section-wrap">
                <div class="row clearfix">
                <div class="portfolio col-3 gutter-less mtn">
                    <div class="portfolio-item default-gallery-item style_2 item-one">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner0.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                    <div class="portfolio-item default-gallery-item style_2 item-two">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner1.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                    <div class="portfolio-item default-gallery-item style_2 item-one item-three">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner2.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                </div>
                <!-- <div class="text-center"><a href="#" class="btn theme-btn mt40">View More </a></div> -->
                </div>
            </div>
        </div>
    </section>    --}}

   {{--  <section>
        <div class="row active-movil" >
                <div class="col-md-12">
                <div class="testimonial-1 "> 
                    <!-- Logo item -->
                    <div><img alt="" src="{{asset('img\products\banner0.png')}}"></div>
                    <div><img alt="" src="{{asset('img\products\banner1.png')}}"></div>
                    <div><img alt="" src="{{asset('img\products\banner2.png')}}"></div>
                </div>
                </div>
            </div>
   </section>  --}}

   <section  class="call-to-action bg-black-pearl disable-movil" style="background-image:url({{ asset('img/contact/banner_purpple.jpg') }});background-size: cover;background-position: center center;">
        <div class="container">
            <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                <div class="big-title-2 text-uppercase animated delay-1s flipInY" style="    display: block; padding: 5%;">
                    <h1>PORTO ALEGRE  es SABOR<br><span> CALIDAD Y VARIEDAD</span></h1>
                </div>
                </div>
            </div>
            </div>
        </div>
    </section>


    

@endsection

