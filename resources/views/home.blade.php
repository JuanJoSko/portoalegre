@extends('layouts.app')

@section('content')

@include('dynamic.slider')

<!--Section:: Testimonial-->
  <section class="testimonial style_2">
        <div class="container bg-white mt60" >
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 about-text">
                        <h2 class="text-center how-text">¿QUIÉNES SOMOS?</h2>
                        <p>Somos  <span>AGUAS FRESCAS</span>  que ofrece a sus clientes y consumidores una experiencia de <span>SABOR;</span> una <span>TRADICIÓN</span> hecha bebida que combina lo mejor de ayer y hoy!
                            Somos una <span>SOLUCIÓN</span> de negocio para nuestros segmentos de <span>CONSUMO</span> que ofrece distintos escenarios de <span>RENTABILIDAD</span>.
                            Somos <span>PRACTICIDAD</span>, <span>VARIEDAD</span> Y <span>CALIDAD</span> en un solo nombre; <span>PORTO ALEGRE</span>.
                            
                            <span>PORTO ALEGRE</span>, empresa 100% <span>MEXICANA</span> con más de 20 años en el mercado desarrollando soluciones de negocios para la Industria de Alimentos y Bebidas.
                            Gracias a su preferencia seguimos creciendo en <span>MÉXICO</span> y <span>ESTADOS UNIDOS</span> ofreciendo <span>AUTÉNTICAS AGUAS FRESCAS</span>.
                        </p>
                        <br>
                        <br>
                        <br>
                        <h4 class="text-center title-testimonial">
                            GRACIAS  a la PREFERENCIA de nuestros CLIENTES y CONSUMIDORES
                        </h4>
                        <div class="mf-counter columns-1 style-2 ">
                            <div class="row fun-fact">
                                <div class="col-md-12 col-sm-12 col-xs-12 counter-column">
                                    <div class="cs-counter-col">
                                    <div class="cs-number-count">
                                        <div class="count-outer"><span class="counter">+</span><span class="blue-color-2 counter count" data-count="320000000">0</span><br>Experiencias de Sabor</div>
                                    </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    {{-- <div class="col-md-6 mt60 video-about disable-movil">
                        <div class="video-responsive">
                            <iframe  src="https://www.youtube.com/embed/sOnqjkJTMaA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                        </div>  
                    </div> --}}
                </div>
            </div>
        </div>
  </section>
    <!--Section:: Testimonial End -->
    <section class="testimonial style_2 bg-white">
        <div class="section-content">
           
            <div class="row">
                <h4 class="text-center title-testimonial">Nuestra razón de ser y estar es la PREFERENCIA de nuestros CLIENTES. <br>
                    GRACIAS por compartir su EXPERIENCIA PORTO ALEGRE.
                </h4>
              <div class="col-md-12">
                  <img class="frange-img" src="{{asset('img/testimonials/testimonial00.png')}}" alt="" srcset="">
                <div class="testimonial_style_2 ">
                    <div class="item">
                        <div  class="testimonial-item style_2 bg_testimonial" style="background-image: url({{asset('img/testimonials/testimonial02.jpg')}})">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="image-logo" src="{{ asset('img/testimonials/logo_1.png') }}" alt="" >
                                </div>
                                <div class="col-md-5">
                                    <div class="testimonial-text">
                                        <p text-center">
                                            "Porto Alegre me ha dado la seguridad de poder ofrecer a mis consumidores productos de calidad gracias a los estándares con los que son elaborados.
                                        </p>
                                        <p text-center">
                                            Esto nos ha permitido poder ofrecer a nuestros clientes bebidas frescas con ingredientes naturales que se adaptan y contribuyen  a la coctelería mexicana."                                </p>
                                        <p class="quote">
                                            - Mixóloga Karen Barrera Morales
                                        </p>
                                        <p class="quote-2">
                                            Especialista en Coctelería Mexicana en Comicx,
                                            La Lupe y Cantina 39
                                        </p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div  class="testimonial-item style_2 bg_testimonial" style="background-image: url({{asset('img/testimonials/testimonial01.jpg')}})">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="image-logo" src="{{ asset('img/testimonials/logo_2.png') }}" alt="" >
                                </div>
                                <div class="col-md-5">
                                    <div class="testimonial-text">
                                            <p >
                                                "Porto Alegre es una solución práctica y sencilla para poder elaborar nuestras Aguas Frescas. Su práctica elaboración nos permite optimizar tiempos en nuestras operaciones. Además, nos damos cuenta de que tienen sabores tradicionalmente mexicanos que nos permite utilizarlo para desarrollar platillos y otras opciones de bebidas.
                                            </p>
                                            <p >
                                                Es una marca que ofrece calidad, sabor, variedad y servicio."
                                            </p>
                                            <p class="quote">
                                                - Juan José López Arellano
                                            </p>
                                            <p class="quote-2">
                                                Chef Ejecutivo de Fiesta Inn                                        
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div  class="testimonial-item style_2 bg_testimonial" style="background-image: url({{asset('img/testimonials/testimonial03.jpg')}})">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="image-logo" src="{{ asset('img/testimonials/logo_3.png') }}" alt="" >
                                </div>
                                <div class="col-md-5">
                                    <div class="testimonial-text">
                                            <p >
                                                "Sin duda una parte fundamental para el buen funcionamiento de una empresa son sus proveedores. Contar con un proveedor confiable y que esté ahí cuando lo necesitas y te resuelva en tiempo los problemas. 
                                            </p>
                                            <p >
                                                Aparte de estar siempre aportado innovación y colaborando en nuevos proyectos con ComicX, ofreciendo sus productos de calidad y a buen costo es lo que siempre agradeceremos a Porto Alegre y nos encanta esa cercanía que tienen en todo momento."
                                            </p>
                                            <p class="quote">
                                                - Ines Armenta Acuña
                                            </p>
                                            <p class="quote-2">
                                                Directora de Administración Comicx                                        
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div  class="testimonial-item style_2 bg_testimonial" style="background-image: url({{asset('img/testimonials/testimonial04.jpg')}})">
                            <div class="row">
                                <div class="col-md-6">
                                    <img class="image-logo" src="{{ asset('img/testimonials/logo_4.png') }}" alt="" >
                                </div>
                                <div class="col-md-5">
                                    <div class="testimonial-text">
                                            <p  >
                                                "Porto Alegre es más que una opción de bebidas y usos alternativos en gastronomía, su producto, su servicio y atención lo convierten en un muy buen partner de negocios." 
                                            </p>
                                            <p class="quote">
                                                - Alfonso Lira
                                            </p>
                                            <p class="quote-2">
                                                Director General de Mochomos Restaurante                                     
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </section>
    <section class="testimonial style_2">
            @include('dynamic.canals')
        </div>
    </section>

    <section  class="call-to-action bg-black-pearl disable-movil" style="background-image:url({{ asset('img/contact/banner_purpple.jpg') }});background-size: cover;background-position: center center;">
        <div class="container">
            <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                <div class="big-title-2 text-uppercase animated delay-1s flipInY" style="    display: block; padding: 5%;">
                    <h1>NUESTRO<br><span>COMPROMISO</span></h1>
                    <p class="text-center">Se aliado de éxito para su empresa o negocio como proveedores de Concentrados para  AGUAS FRESCAS. 
                        Dentro de nuestra oferta de VALOR incluye ofrecer un PRODUCTO que cumpla con los estándares de CALIDAD y ofrezca modelo de RENTABILIDAD y SERVICIO que satisfaga su demanda y requerimientos de sus consumidores.
                    </p>
                </div>
                </div>
            </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="img-sections active-movil">
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/banner_yellow.jpg') }}" alt="">
                            <div class="centered">
                                <h1>NUESTRA PROPUESTA<br><span>DE VALOR</span></h1>
                                <p class="disable-movil">Producto, Calidad y Servicio son nuestras propuesta de valor para su empresa y/o negocio.  Buscando la oportunidad de poder servirle en México y  E.U.A. ofreciendo las auténticas Aguas Frescas</p>
                            </div>
                        </div>
                    </div>
                    <div class="product-panel" >
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/yellow.png') }}" alt="">

                            <div class="img-color ">
                                <div class="item-content animated delay-2s zoomIn info">
                                    <div class="icon-title">
                                        <span class="icon icon-product"></span>
                                        <p>PRODUCTOS</p>
                                    </div>
                                    <p class="icon-info">Son elaborados con pulpa e ingredientes 100% de origen natural. Más de 15 sabores diferentes.</p>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/orange.png') }}" alt="">
                            <div class="img-color">
                                <div class="item-content animated delay-2s zoomIn info">
                                    <div class="icon-title">
                                        <span class="icon icon-quality"></span>
                                        <p>CALIDAD</p>
                                    </div>
                                    <p class="icon-info">Nuestro compromiso de Calidad Total busca siempre innovar y ofrecer satisfacer las necesidades de nuestros clientes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-panel">
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/orange.png') }}" alt="">
                            <div class="img-color">
                                <div class="item-content animated delay-2s zoomIn info">
                                    <div class="icon-title">
                                        <span class="icon icon-service"></span>
                                        <p>SERVICIO</p>
                                    </div>
                                    <p class="icon-info">Contamos con atención personalizada en toda la República Mexicana y parte de Estados Unidos. Disponemos de una red de logística y servicio de mantenimiento.</p>
                                </div>
                            </div>
                        </div>
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/yellow.png') }}" alt="">
                            <div class="img-color">
                                <div class="item-content animated delay-2s zoomIn info">
                                    <div class="icon-title">
                                        <span class="icon icon-rent"></span>
                                        <p>RENTABILIDAD</p>
                                    </div>
                                    <p class="icon-info">Asesoramos tu crecimiento para nuestros clientes y aliados que buscan una bebida rentable, deliciosa y práctica.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="img-sections disable-movil">
                        <div class="product-content">
                            <img class="img-colors" src="{{ asset('/img/colors/banner_yellow.jpg') }}" alt="">
                            <div class="centered">
                                <h1>NUESTRA PROPUESTA<br><span>DE VALOR</span></h1>
                                <p class="disable-movil">Producto, Calidad y Servicio son nuestras propuesta de valor para su empresa y/o negocio.  Buscando la oportunidad de poder servirle en México y  E.U.A. ofreciendo las auténticas Aguas Frescas</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial active-movil">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div  class="testimonial-item  bg_usa" style="background-repeat: no-repeat; background-image: url({{ asset('/img/colors/banner_purple.jpg') }})">
                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title text-uppercase animated delay-2s rubberBand text-center">
                                    <h1>UNO PARA CADA<br><span>NECESIDAD</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

  <section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="color-panel disable-movil">
                    <div class="single-what-we-do clearfix">
                        <div class="img-wrap fix-width">
                            <a href="{{ route('products') }}">
                                <img src="{{ asset('/img/colors/color_00.jpg') }}" alt="">
                            </a>
                        </div>
                    </div>
                    {{--  <img class="img-colors" src="{{ asset('/img/colors/color_00.jpg') }}" alt="">  --}}
                </div>
                <div class="color-panel disable-movil">
                    <div class="portfolio-item default-gallery-item style_2 item-one" >
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <figure class="image-box " >
                                <img src="{{ asset('/img/colors/banner_purple.jpg') }}" class="img-responsive">
                                <div class="top-text">
                                    <span>CONCENTRADOS / FOOD SERVICE</span>
                                </div>
                                <div class="lightbox-content-usa-2 fix-lightbox-content animated delay-2s fadeIn">
                                    <h1>UNO PARA<br>CADA<br><span>NECESIDAD</span></h1>
                                </div>
                                <div class="bottom-text">
                                    <span>BEBIDAS</span>
                                </div>
                            </figure>
                            <!--Overlay Box-->
                        </div>                          
                    </div>
                </div>
                <div  class="img-sections">
                    <div class="col-md-12 fix-colum">
                        <div class="single-what-we-do clearfix">
                            <div class="img-wrap fix-width">
                                <a href="{{ route('products') }}">
                                    <img src="{{ asset('/img/colors/section_00.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="single-what-we-do clearfix">
                            <div class="img-wrap fix-width">
                                <a href="{{ route('products') }}">
                                    <img src="{{ asset('/img/colors/section_01.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="single-what-we-do clearfix">
                            <div class="img-wrap fix-width">
                                <a href="{{ route('products') }}">
                                    <img src="{{ asset('/img/colors/section_02.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="single-what-we-do clearfix active-movil">
                            <div class="img-wrap fix-width">
                                <a href="{{ route('products') }}">
                                    <img src="{{ asset('/img/colors/section_03.jpg') }}" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
  </section>
  <!-- Section::  -->
    <section class="testimonial active-movil">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div  class="testimonial-item  bg_usa" style="background-repeat: no-repeat; background-image: url({{asset('img/colors/banner_red.jpg')}})">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title text-uppercase animated delay-2s rubberBand text-center animated delay-2s fadeIn">
                                    <h1>DIVERSIDAD<br><span>DE USOS</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-prescribe">
                        <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <a href="{{ asset('/pdfs/aguas_frescas.pdf') }}" download>
                                    <figure class="image-box " >
                                        <img src="{{ asset('/img/colors/waters.jpg') }}" class="img-responsive">
                                        <div class="lightbox-content-usa">
                                            <h1>AGUAS FRESCAS<br><span></span></h1>
                                        </div>
                                        <div class="pdf-content animated infinite delay-6s bounce">
                                            <img class="pdf-download" src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                        </div>
                                    </figure>
                                </a>
                                <!--Overlay Box-->
                            </div>                          
                        </div>

                        <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <a href="{{ asset('/pdfs/cocteleria.pdf') }}" download>
                                    <figure class="image-box " >
                                        <img src="{{ asset('/img/colors/seafood.jpg') }}" class="img-responsive">
                                        <div class="lightbox-content-usa">
                                            <h1>COCTELERÍA<br><span></span> </h1>
                                        </div>
                                        <div class="pdf-content animated infinite delay-6s bounce">
                                            <img class="pdf-download" src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                        </div>
                                    </figure>
                                </a>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                        <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <a href="{{ asset('/pdfs/aderezos.pdf') }}" download>
                                    <figure class="image-box " >
                                        <img src="{{ asset('/img/colors/dressings.jpg') }}" class="img-responsive">
                                        <div class="lightbox-content-usa">
                                            <h1>ADEREZOS<br><span></span></h1>
                                        </div>
                                        <div class="pdf-content animated infinite delay-6s bounce">
                                            <img class="pdf-download " src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                        </div>
                                    
                                    </figure>
                                </a>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                    </div>
                    <div class="col-prescribe-banner disable-movil">
                        <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box " >
                                    <img src="{{ asset('/img/colors/banner_red.jpg') }}" class="img-responsive">
                                    <div class="lightbox-content-usa-2 animated delay-2s zoomIn">
                                        <h1 class="fix-lightbox-content-usa-2">DIVERSIDAD<br><span>DE USOS</span></h1>
                                    </div>
                                
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--  <section>
        <div class="row">
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_aib.jpg') }}" alt="">
            </div>
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_food.jpg') }}" alt="">
            </div>
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_esr.jpg') }}" alt="">
            </div>
        </div>
    </section>  --}}

@endsection
