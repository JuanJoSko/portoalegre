<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/jpg" href="{{ asset('/img/favicon-porto.jpg   ') }}"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Icon -->
    <link href="{{ asset('css/stroke-gap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icofont.css') }}" rel="stylesheet">
    <!-- Fancybox -->
    <link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet">
    <!-- Revolution Slider -->
    <link href="{{ asset('css/revolution-slider.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ asset('css/main-style.css') }}" rel="stylesheet">
    <!-- Responsive -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('/font/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">


</head>
    <body class="bg-porto">
        <div class="page-wrapper"> 
            @include('static.header')

            @yield('content')
            @include('static.footer')

        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery.js') }}"></script> 
        <script src="{{ asset('js/jquery-ui-1.11.4/jquery-ui.js') }}"></script> 
        <script src="{{ asset('js/revolution.min.js') }}"></script> 
        <script src="{{ asset('js/rev-custom.js') }}"></script>
        <script src="{{ asset('js/all-jquery.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
        <script src="{{ asset('js/script.js') }}"></script>
        <script src="//code.tidio.co/p6pyzmldz9jyizqrecm7fuktjded6peq.js"></script>
        <script>
                $( document ).ready(function() {
                     var pathCurrent = window.location.href;
                     $('.nav-link').each(function (index, value) {
                        if($(this).attr('href') == window.location.href){
                             $(this).addClass('current');
                        }else{
                            $(this).removeClass('current');
                        }
                     });
                     var lastScroll = 0;
  
                     {{-- $(window).scroll(function () {
                        var currentScroll = $(document).scrollTop();

                        if (currentScroll < lastScroll) {
                            $('.header-mainbox ').css('display','block');
                        
                        } else {
                            $('.header-mainbox ').css('display','none');
                        }
                       
                        if(currentScroll <= 300){
                            $('.header-mainbox ').css('display','block');

                        }
                        lastScroll = currentScroll;
                    });  --}}
                  
                    
                });
        </script>
        @yield('scripts')
        
    </body>
</html>
