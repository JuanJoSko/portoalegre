<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin - {{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/jpg" href="{{ asset('/img/favicon-porto.jpg   ') }}"/>
    <link href="{{ asset('/admin/css/main.css') }}" rel="stylesheet"></head>
    <script type="text/javascript" src="{{ asset('/admin/scripts/main.js') }}"></script></body>
    <link rel="stylesheet" href="dist/css/dropify.min.css">

</head>
    <body>
        <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
            @include('static.admin.header')

            @yield('content')
            @include('static.admin.footer')
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        @yield('scripts')
        
    </body>
</html>
