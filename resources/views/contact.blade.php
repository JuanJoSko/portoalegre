@extends('layouts.app')

@section('content')
    <main class="bg-white">
        <!-- Header -->
        <div
            
            style="background-image: url('{{asset('/img/contact/banner_contact.jpg')}}'); width:100%; background-repeat: no-repeat;">
                <div class="row" >
                    <div class="col-sm-6 col-xs-12 ml-60 m-b-4 banner-text">
                        <div class="product-title  animated delay-1s flipInY">
                            <h1>SIEMPRE<br><span>CERCA DE TI</span></h1>
                            <div class="banner-description disable-movil">
                                <p >Nuestro compromiso de  SERVICIO Y ATENCIÓN es poder ofrecerle productos siempre listos y siempre disponibles para su negocio y/o empresa.</p>
                                <br>
                                <p>
                                    Por ello, trabajamos día a día para brindarle la certeza de llegar hasta usted ofreciendo lo mejor de nosotros. 
                                </p>
                                <br>
                                <p>
                                    Nuestra cobertura dentro de México y Estados Unidos pone en manifiesto el esfuerzo en el que seguimos trabajando haciendo llegar a usted los mejores concentrados para la elaboración de AGUAS FRESCAS.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </main>
    <section class="testimonial style_2 mt60 mb60 active-movil">
            <div class="section-content">
            <div class="row">
                <div class="container">
                    <div class="col-md-6 about-text text-center">
                        <p class="blue-color jsregular-18">Ubicados estratégicamente para poder brindar el mejor servicio de entrega tanto en México como en E.U.A</p>
                    </div>
                </div>
                
            </div>
        </div>
      </section>
    <div class="bg-white" style="top: 0px;
    position: relative;" class="disable-movil">
        <div class="map-title text-uppercase animated delay-1s flipInY">
            <h1>Estamos para<br><span>atenderte</span></h1>
            <div class="map-subtitle">
                <img src="{{ asset('/img/contact/marker.png') }}" alt="" srcset="">
                <h2>Cedis <span>Porto Alegre</span></h2>
            </div>
            <div class="map-subtitle">
                <span class="circle"></span>
                <h2>Area de cobertura <span>México</span></h2>
            </div>
            <div class="clear-fix"></div>
            <div class="map-subtitle" style="margin-top: 33px;">
                <span class="circle green"></span>
                <h2>Area de cobertura <span>EUA</span></h2>
            </div>
        </div>
        @include('dynamic.map')
    </div>

    <div class="section-content active-movil">
        <div class="row">
            <div class="col-md-4">
            <div class="sidebar">               
                <!-- Services Widget -->
                <div class="sidebar-widget services-widget yellow">
                    <div class="col-md-4 pn ">
                        <div class="item">
                            <div class="img-color">
                                <div class="item-bg" style="background-image: url(http://portoalegre.loc/img/colors/banner_yellow.jpg)">
                                    <div class="big-title-2  text-uppercase animated delay-2s flipInY info">
                                        <h1><span>MÉXICO</span></h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="sidebar-title">
                    <h3>Contanos con área de cobertura nacional <br>Para siempre estar cerca de tí</h3>
                    <br>
                    
                </div>
                <h3>Ubica el Centro de  Distribución más cercano <br>a tu localidad</h3>
                <ul class="law-list pb20">
                    <li>
                        <a class="active" href="javascript:;">Baja California</a>
                        <p>TIJUANA, MEXICALI, SAN LUIS RIO</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">SONORA</a>
                        <p>HERMOSILLO, OBREGON, GUAYMAS</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">CHIHUAHUA</a>
                        <p>CHIHUAHUA</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">NUEVO LEÓN</a>
                        <p>MONTERREY</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">SINALOA</a>
                        <p>CULIACAN</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">SINALOA</a>
                        <p>CULIACAN</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">JALISCO</a>
                        <p>GUADALAJARA</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">GUANAJUATO</a>
                        <p>LEON</p>
                    </li>
                    <li>
                        <a class="active" href="javascript:;">QUERETARO</a>
                        <p>QUERETARO</p>
                    </li>
                    <li>
                        <span class="icon icon-phone"></span>
                        <p class="mt20">(662) 108 18 50</p>
                        <div class="row mt60">
                            <span class="icon icon-email"></span>
                            <p class="mt20">atención.clientes@portoalegre.com.mx</p>
                        </div>
                    </li>
                </ul>
                </div>
                <div class="sidebar-widget services-widget green">
                        <div class="col-md-4 pn ">
                            <div class="item">
                                <div class="img-color">
                                    <div class="item-bg" style="background-image: url(http://portoalegre.loc/img/contact/green.jpg)">
                                        <div class="big-title-2  text-uppercase animated delay-2s flipInY info">
                                            <h1><span>USA</span></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="sidebar-title">
                        <h3>Contanos con área de cobertura nacional <br>Para siempre estar cerca de tí</h3>
                        <br>
                        
                    </div>
                    <h3>Ubica el Centro de  Distribución más cercano <br>a tu localidad</h3>
                    <ul class="law-list pb20">
                        <li>
                            <a class="active" href="javascript:;">TEXAS</a>
                            <p>EL PASO, ARIZONA TUCSON, PHOENIX</p>
                        </li>
                       
                        <li>
                            <a class="active" href="javascript:;">NEVADA</a>
                            <p>LAS VEGAS</p>
                        </li>
                        <li>
                            <a class="active" href="javascript:;">UTAH</a>
                            <p>UTAH</p>
                        </li>
                        <li>
                            <a class="active" href="javascript:;">CALIFORNIA</a>
                            <p>LOS ANGELES, VENTURA</p>
                        </li>
                        <li>
                           
                                <span class="icon icon-phone"></span>
                                <p class="mt20">(662) 108 18 50</p>
                                <div class="row mt60">
                                    <span class="icon icon-email"></span>
                                    <p class="mt20">sales.usa@portoalegre.com.mx</p>
                                </div>
                        </li>
                    </ul>
                    </div>          
            </div>
            </div>
        </div>
    </div>


    <section  class="call-to-action bg-black-pearl disable-movil" style="background-image:url({{ asset('img/contact/banner_purpple.jpg') }});background-size: cover;background-position: center center;">
        <div class="container">
          <div class="section-content">
            <div class="row">
              <div class="col-md-12">
                <div class="big-title-2 text-uppercase animated delay-1s flipInY" style="    display: block; padding: 5%;">
                    <h1>Estar en contacto contigo<br><span>Es nuestra prioridad</span></h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    {{--  <section id="contactForm" class="contact-1"  style="    background: #288a4a;">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6">
                    <img style="width: 100%;" src="{{ asset('img/contact/call.png') }}" alt="">
                </div>
                <div class="col-md-6 ">
                    <!-- Contact Form -->
                    <form class="contact-form pt90" method="post" enctype="multipart/form-data" action="/upload" role="form" novalidate="true">
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_name">Nombre</label>
                                        <input id="form_name" type="text" name="name" class="form-control"  required="required" data-error="El nombre es obligatorio.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_email">Correo</label>
                                        <input id="form_email" type="email" name="email" class="form-control"  required="required" data-error="El email es invalido.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_phone">Teléfono</label>
                                        <input id="form_phone" type="text" name="phone" class="form-control"  required="required" data-error="El telefono es obligatorio.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="form_phone">Asunto</label>
                                <div class="form-group">
                                    <input id="form_phone" type="text" name="subject" class="form-control"  required="required" data-error="El telefono es obligatorio.">

                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Mensaje *</label>
                                        <textarea id="form_message" name="message" class="form-control"  rows="9" required="" data-error="¡Por favor! escribanos un Mensaje."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 " >
                                    <button type="submit" class="btn btn-success btn-send pull-right" >ENVIAR</button>
                                </div>

                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </section>  --}}
    
    
@endsection

@section('scripts')
    <script src="{{ asset('/js/map.js') }}"></script>
@endsection

