<!-- Slider::Section Start-->
  <section class="main-slider style_2">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
            <li data-transition="fade" data-slotamount="1" data-masterspeed="1000"  data-saveperformance="off" >  <img src="{{ asset('img/banners/banner01.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
              <div class="tp-caption lfb tp-resizeme"
                  data-x="left" 
                  data-hoffset="20"
                  data-y="center" 
                  data-voffset="0"
                  data-speed="1500"
                  data-start="750"
                  data-easing=""
                  data-splitin="none"
                  data-splitout="none"
                  data-elementdelay="0.01"
                  data-endelementdelay="0.3"
                  data-endspeed="1200"
                  data-endeasing="Power4.easeIn"
                  style="z-index: 4; white-space: nowrap;">
              <div class="text-uppercase home-title">
				<h1 class="disable-movil">CONCENTRAMOS<br><span>EL SABOR<br>DE MÉXICO </span></h1>
                <h1 class="active-movil text-center">CONCENTRAMOS<br><span>EL SABOR<br>DE MÉXICO </span></h1>
				
              </div>
            </div>
		  </li>
		  <li data-transition="slideup" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner05.jpg') }}"  data-saveperformance="off"  > <img src="{{ asset('img/banners/banner03.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="right" 
                        data-hoffset="-20"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
              <div class="home-title text-left text-uppercase">
                <h1 class="disable-movil">MIXOLOGÍA FÁCIL<br><span>Y DELICIOSAS</span><br>CREACIONES</h1>
                <h1 class="active-movil text-center" style="margin-left:-20%;">MIXOLOGÍA FÁCIL<br><span>Y DELICIOSAS</span><br>CREACIONES</h1>
              </div>
            </div>
          </li>
          <li data-transition="slideup" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner05.jpg') }}"  data-saveperformance="off"  > <img src="{{ asset('img/banners/banner05.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="center" 
                        data-hoffset="-15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
              <div class="home-title text-left text-uppercase">
				<h1 class="disable-movil">PREPARALO A TU GUSTO<br><span>CON UN TOQUE NATURAL</span></h1>
				<h1 class="active-movil text-center">PREPARALO <br>A TU GUSTO<br><span>CON UN TOQUE <br> NATURAL</span></h1>

              </div>
            </div>
          </li>
          <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner04.jpg') }}"  data-saveperformance="off"  > <img src="{{ asset('img/banners/banner04.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="left" 
                        data-hoffset="20"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                <div class="home-title text-uppercase">
					<h1 class="text-left disable-movil">AUTÉNTICAS<br><span>AGUAS FRESCAS</span></h1>
                    <h1 class="text-center active-movil">AUTÉNTICAS<br><span>AGUAS FRESCAS</span></h1>
					
                </div>
            </div>
          </li>
        </ul>
        <div class="tp-bannertimer"></div>
      </div>
    </div>
    <div class="col-md-8 co-sm-8 pull-right active-movil social-movil">
		<ul class="footer-social">
			<li>
				<a target="_blank" href="http://www.facebook.com/PortoAlegreBebidas">
					<i class="fa fa-facebook"></i>
				</a>
			</li>
			<li><a target="_blank" href="http://www.instagram.com/PortoAlegreBebidas_of">
				<i class="fa fa-instagram"></i>
				</a>
			</li>
			<li><a target="_blank" href="https://twitter.com/PortoAlegre_of">
				<i class="fa fa-twitter"></i>
				</a>
			</li>
			<li><a target="_blank" href="https://www.linkedin.com/company/portoalegrebebidas">
				<i class="icon icon-in"></i>
				</a>
			</li>
			<li><a target="_blank" href="https://wa.me/5216622333816">
				<i class="fa fa-whatsapp"></i>
				</a>
			</li>
		</ul>
	</div>
  </section>
  <!-- Slider::Section End--> 