<!-- Slider::Section Start-->
  <section class="main-slider style_2">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
          <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner01.jpg') }}"  data-saveperformance="off" >  <img src="{{ asset('img/banners/banner01.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="left" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="750"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
              <div class="big-title text-uppercase">
                <h1 class="banner-title">FRESH AND<br><span class="banner-span">INNOVATIVE</span></h1>
              </div>
            </div>
          </li>
          <li data-transition="slideup" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner05.jpg') }}"  data-saveperformance="off"  > <img src="{{ asset('img/banners/banner05.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="left" 
                        data-hoffset="-15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
              <div class="big-title text-left text-uppercase">
                <h1  class="banner-title">AUTHENTIC<br><span  class="banner-span">AGUAS FRESCAS</span></h1>
              </div>
            </div>
          </li>
          <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner04.jpg') }}"  data-saveperformance="off"  > <img src="{{ asset('img/banners/banner04.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme"
                        data-x="left" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                <div class="big-title text-left text-uppercase">
                    <h1  class="banner-title">100% MEXICAN<br><span  class="banner-span">FLAVORS</span></h1>
                </div>
            </div>
          </li>
          <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="{{ asset('img/banners/banner03.jpg') }}"  data-saveperformance="off" > <img src="{{ asset('img/banners/banner03.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption lfb tp-resizeme disable-movil"
                        data-x="left" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                <div class="big-title text-left text-uppercase">
                    <h1  class="banner-title">GREAT<br><span  class="banner-span">BEVERAGE EXPERIENCE</span></h1>
                </div>
			</div>
			<div class="tp-caption lfb tp-resizeme active-movil"
				data-x="center" 
				data-hoffset="20"
				data-y="center" 
				data-voffset="0"
				data-speed="1500"
				data-start="750"
				data-easing=""
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.01"
				data-endelementdelay="0.3"
				data-endspeed="1200"
				data-endeasing="Power4.easeIn"
				style="z-index: 4; white-space: nowrap;">
				<div class="text-uppercase home-title">
					<h1 >GREAT<br><span>BEVERAGE<br>EXPERIENCE</span></h1>
				  </div>
            </div>
          </li>
        </ul>
        <div class="tp-bannertimer"></div>
      </div>
    </div>
  </section>
  <!-- Slider::Section End--> 