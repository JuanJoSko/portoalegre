<!-- Slider::Section Start-->
  <section class="canal-slider style_2 ">
    <div class="tp-banner-container">
      <div class="tp-banner">
        <ul>
            <div class="lightbox-content-usa-2 fix-canal-content">
                <div class="row ">
                    <div class="canal">
                        <h3 class="jsLightItalic-30">NUESTROS CLIENTES:</h3>
                    </div>
                </div>
            </div>
            <li 
                data-transition="slideup" 
                data-slotamount="1" 
                data-masterspeed="1000" 
                data-thumb="{{ asset('img/canals/canal00.jpg') }}"
            >  
            <img src="{{ asset('img/canals/canal00.jpg') }}"
                data-bgposition="center center" 
                data-bgfit="cover" 
                data-bgrepeat="no-repeat"
            >
            <div class="tp-caption fade tp-resizeme disable-movil"
                data-x="center" 
                data-hoffset="15"
                data-y="center" 
                data-voffset="-50"
                data-speed="1500"
                data-start="750"
                data-easing=""
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.01"
                data-endelementdelay="0.3"
                data-endspeed="1200"
                data-endeasing="Power4.easeIn"
                style="z-index: 4; white-space: nowrap;"
            >
                <div class="row">
                    <h2 id="" class="canal-title titleCanal">CAFETERÍAS</h2>
                    <div class="text-center canal-description">
                        <p>¡Ofrecer calidad y diversidad es posible! <p>
                        <p>Nuestra oferta de sabores hace que la experiencia</p>
                        <p>de tus consumidores sea dinámica y versátil. ¡Siempre Fresca, Siempre Lista! </p>
                    </div>
                </div>
            </div>
            <div class="tp-caption fade tp-resizeme active-movil"
                data-x="center" 
                data-hoffset="15"
                data-y="center" 
                data-voffset="-50"
                data-speed="1500"
                data-start="750"
                data-easing=""
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.01"
                data-endelementdelay="0.3"
                data-endspeed="1200"
                data-endeasing="Power4.easeIn"
                style="z-index: 4; white-space: nowrap;"
            >
                    <div style="margin-top:-50%; margin-left:20%;" class="row">
                        <h2 id="" class="canal-title titleCanal">CAFETERÍAS</h2>
                    </div>
            </div>

          
            <div class="tp-caption skewfromleft tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;">
                <div>
                    <img class="fix-logos" src="{{ asset('img/canals/logos00.jpg') }}" alt="">
                </div>
            </div>
            </li>
            <li 
                data-transition="slideup" 
                data-slotamount="1" 
                data-masterspeed="1000" 
                data-thumb="{{ asset('img/canals/canal01.jpg') }}"
            >  
            <img src="{{ asset('img/canals/canal01.jpg') }}"
                data-bgposition="center center" 
                data-bgfit="cover" 
                data-bgrepeat="no-repeat"
            >
            <div class="tp-caption fade tp-resizeme disable-movil"
                data-x="center" 
                data-hoffset="15"
                data-y="center" 
                data-voffset="-50"
                data-speed="1500"
                data-start="750"
                data-easing=""
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.01"
                data-endelementdelay="0.3"
                data-endspeed="1200"
                data-endeasing="Power4.easeIn"
                style="z-index: 4; white-space: nowrap;"
            >
                    <div class="row">
                        <h2 id="" class="canal-title titleCanal">CASINOS</h2>
                        <div class="text-center canal-description">
                            <p>El Placer se combina con los mejores SABORES<p>
                            <p>para ofrecer a tus consumidores diversidad,</p>
                            <p>servicio y una bebida llena de sabor.</p>
                        </div>
                    </div>
            </div>
            <div class="tp-caption fade tp-resizeme active-movil"
                data-x="center" 
                data-hoffset="15"
                data-y="center" 
                data-voffset="-50"
                data-speed="1500"
                data-start="750"
                data-easing=""
                data-splitin="none"
                data-splitout="none"
                data-elementdelay="0.01"
                data-endelementdelay="0.3"
                data-endspeed="1200"
                data-endeasing="Power4.easeIn"
                style="z-index: 4; white-space: nowrap;"
            >
                    <div style="margin-top:-50%; margin-left:50%;" class="row">
                        <h2 id="" class="canal-title titleCanal">CASINOS</h2>
                    </div>
            </div>
           
            <div class="tp-caption skewfromleft tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;">
                <div>
                    <img class="fix-logos" src="{{ asset('img/canals/logos01.jpg') }}" alt="">
                </div>
            </div>
          </li>
            <li 
                data-transition="slideup" 
                data-slotamount="1" 
                data-masterspeed="1000" 
                data-thumb="{{ asset('img/canals/canal02.jpg') }}"
            >  
                <img src="{{ asset('img/canals/canal02.jpg') }}"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption fade tp-resizeme disable-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div class="row">
                            <h2 id="" class="canal-title titleCanal">COMEDORES INDUSTRIALES</h2>
                            <div class="text-center canal-description">
                                <p>Servicio y Diversidad es nuestra mejor carta de presentación pues</p>
                                <p>el SABOR existe desde, siempre. Optimizamos tus costos y facilitamos</p>
                                <p>tus operaciones para que tu experiencia de alto consumo sea rentable.</p>
                            </div>
                        </div>
                </div>
                <div class="tp-caption fade tp-resizeme active-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div style="margin-top:-50%; margin-left:15%;" class="row">
                            <h2 style="line-height: 92px;" class="canal-title titleCanal">COMEDORES<br>INDUSTRIALES</h2>
                        </div>
                </div>
                    <div class="tp-caption skewfromleft tp-resizeme"
                        data-x="center" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="200"
                        data-speed="1500"
                        data-start="750"
                        data-easing=""
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                    <div>
                        <img class="fix-logos" src="{{ asset('img/canals/logos02.jpg') }}" alt="">
                    </div>
                </div>
            </li>
            <li 
                data-transition="slideup"
                data-slotamount="1"
                data-masterspeed="1500"
                data-thumb="{{ asset('img/canals/canal03.jpg') }}"
                data-saveperformance="off"
            > 
                <img src="{{ asset('img/canals/canal03.jpg') }}"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption fade tp-resizeme disable-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div class="row">
                            <h2 id="" class="canal-title titleCanal">HOTELES</h2>
                            <div class="text-center canal-description">
                                <p>Nuestro compromiso es ofrecer diversidad de opciones con toque natural para sus clientes y</p>
                                <p>huéspedes. Buscamos  potencializar nuestra marca hacia otros usos y diversidad de</p>
                                <p>aplicaciones que sirvan de inspiración para las mejores creaciones de AGUAS FRESCAS.</p>
                            </div>
                        </div>
                </div>
                <div class="tp-caption fade tp-resizeme active-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div style="margin-top:-50%; margin-left:50%;" class="row">
                            <h2 id="" class="canal-title titleCanal">HOTELES</h2>
                        </div>
                </div>
               
                <div class="tp-caption skewfromleft  tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="">
                    <img class="fix-logos" src="{{ asset('img/canals/logos03.jpg') }}" alt="">
                </div>
                </div>
            </li>
            <li 
                data-transition="slideup"
                data-slotamount="1"
                data-masterspeed="1500"
                data-thumb="{{ asset('img/canals/canal04.jpg') }}"
                data-saveperformance="off"
            > 
                <img src="{{ asset('img/canals/canal04.jpg') }}"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption fade tp-resizeme disable-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div class="row">
                            <h2 id="" class="canal-title titleCanal">TIENDAS DE CONVENIENCIA</h2>
                            <div class="text-center canal-description">
                                <p>Buscamos garantizar nuestra oferta de BEBIDAS LISTAS PARA TOMAR en este segmento de</p>
                                <p>NEGOCIO. Gracias a la preferencia de nuestros CONSUMIDORES seguimos creciendo y</p>
                                <p>conquistando paladares en todas las familias mexicanas.</p>
                            </div>
                        </div>
                </div>
                <div class="tp-caption fade tp-resizeme active-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div style="margin-top:-50%; margin-left:5%;" class="row">
                            <h2 style="line-height: 92px;" class="canal-title titleCanal">TIENDAS DE<br>CONVENIENCIA</h2>
                        </div>
                </div>
                <div class="tp-caption skewfromleft  tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="">
                    <img class="fix-logos" src="{{ asset('img/canals/logos04.jpg') }}" alt="">
                </div>
                </div>
            </li>
            <li 
                data-transition="slideup"
                data-slotamount="1"
                data-masterspeed="1500"
                data-thumb="{{ asset('img/canals/canal05.jpg') }}"
                data-saveperformance="off"
            > 
                <img src="{{ asset('img/canals/canal05.jpg') }}"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption fade tp-resizeme disable-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div class="row">
                            <h2 id="" class="canal-title titleCanal">RESTAURANT & BAR</h2>
                            <div class="text-center canal-description">
                                <p>Sabemos que lo más valioso es ofrecer experiencias en alimentos y bebidas. Personaliza la</p>
                                <p>experiencia de una auténtico sabor ofreciendo  una auténtica AGUA FRESCA con ingredientes</p>
                                <p>naturales</p>
                            </div>
                        </div>
                </div>
                <div class="tp-caption fade tp-resizeme active-movil"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="-50"
                    data-speed="1500"
                    data-start="750"
                    data-easing=""
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0.01"
                    data-endelementdelay="0.3"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                        <div style="margin-top:-30%; " class="row">
                            <h2 id="" class="canal-title titleCanal">RESTAURANT & BAR</h2>
                        </div>
                </div>
                <div class="tp-caption skewfromleft  tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="">
                    <img class="fix-logos" src="{{ asset('img/canals/logos05.jpg') }}" alt="">
                </div>
                </div>
            </li>
        </ul>
        <div class="tp-bannertimer"></div>
      </div>
    </div>
  </section>
  <!-- Slider::Section End--> 
{{--  <section class="main-slider">
    <div class="tp-banner-container">
      <div class="tp-banner">
           
        <ul>
            <div class="lightbox-content-usa-2 fix-canal-content">
                <div class="row ">
                    <div class="canal">
                        <h3 class="jsLightItalic-30">NUESTROS CLIENTES:</h3>
                    </div>
                </div>
                <div class="row">
                    <h2 id="" class="canal-title jsLightItalic-150 titleCanal">CASINOS</h2>
                </div>
            </div>
            <li 
                data-transition="slideup" 
                data-slotamount="1" 
                data-masterspeed="1500" 
                data-thumb="{{ asset('img/canals/canal01.jpg') }}"  
                data-saveperformance="off"  
            > 
                <img src="{{ asset('img/canals/canal01.jpg') }}"
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption skewfromleft tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="link-btn">
                    <img class="fix-logos" src="{{ asset('img/canals/logos01.png') }}" alt="">
                </div>
                </div>
            </li>
            <li 
                data-transition="slideup"
                data-slotamount="1"
                data-masterspeed="1500"
                data-thumb="{{ asset('img/canals/canal02.jpg') }}"
                data-saveperformance="off"
                data-title="We Works With"
            > 
                <img src="{{ asset('img/canals/canal02.jpg') }}"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption skewfromleft  tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="">
                    <img class="fix-logos" src="{{ asset('img/canals/logos02.png') }}" alt="">
                </div>
                </div>
            </li>
            <li 
                data-transition="slideup"
                data-slotamount="1"
                data-masterspeed="1500"
                data-thumb="{{ asset('img/canals/canal03.jpg') }}"
                data-saveperformance="off"
                data-title="We Works With"
            > 
                <img src="{{ asset('img/canals/canal03.jpg') }}"
                    data-bgposition="center center"
                    data-bgfit="cover"
                    data-bgrepeat="no-repeat"
                >
                <div class="tp-caption skewfromleft  tp-resizeme"
                    data-x="center" 
                    data-hoffset="15"
                    data-y="center" 
                    data-voffset="200"
                    data-speed="1500"
                    data-start="750"
                    data-easing="Power4.easeOut"
                    data-splitin="none"
                    data-splitout="none"
                    data-elementdelay="0"
                    data-endelementdelay="0"
                    data-endspeed="1200"
                    data-endeasing="Power4.easeIn"
                    style="z-index: 4; white-space: nowrap;"
                >
                <div class="">
                    <img class="fix-logos" src="{{ asset('img/canals/logos03.png') }}" alt="">
                </div>
                </div>
            </li>
            <li 
            data-transition="slideup" 
            data-slotamount="1" 
            data-masterspeed="1000" 
            data-thumb="{{ asset('img/canals/canal04.jpg') }}" 
            data-saveperformance="off"  > <img src="{{ asset('img/canals/canal04.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption skewfromleft tp-resizeme"
                        data-x="left" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="750"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                <div class="home-title text-uppercase">
                    <img class="fix-logos" src="{{ asset('img/canals/logos04.png') }}" alt="">
                </div>
            </div>
          </li>
            <li 
            data-transition="slideup" 
            data-slotamount="1" 
            data-masterspeed="1000" 
            data-thumb="{{ asset('img/canals/canal04.jpg') }}" 
            data-saveperformance="off"  > <img src="{{ asset('img/canals/canal05.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
            <div class="tp-caption skewfromleft tp-resizeme"
                        data-x="left" 
                        data-hoffset="15"
                        data-y="center" 
                        data-voffset="0"
                        data-speed="1500"
                        data-start="750"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; white-space: nowrap;">
                <div class="home-title text-uppercase">
                    <img class="fix-logos" src="{{ asset('img/canals/logos05.png') }}" alt="">
                </div>
            </div>
          </li>
        </ul>
        <div class="tp-bannertimer"></div>
      </div>
    </div>
</section>  --}}
