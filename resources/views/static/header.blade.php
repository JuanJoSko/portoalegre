<!-- Main Header / Style One-->
<header class="main-header header-style-two">
	<!--Header-Main Box-->
	<div class="header-mainbox style_3">
		<div class="container ptn pbn">
			<div class="col-md-8 co-sm-8 pull-right disable-movil">
				<ul class="footer-social">
					<li>
						<a target="_blank" href="http://www.facebook.com/PortoAlegreBebidas">
							<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li><a target="_blank" href="http://www.instagram.com/PortoAlegreBebidas_of">
						<i class="fa fa-instagram"></i>
						</a>
					</li>
					<li><a target="_blank" href="https://twitter.com/PortoAlegre_of">
						<i class="fa fa-twitter"></i>
						</a>
					</li>
					<li><a target="_blank" href="https://www.linkedin.com/company/portoalegrebebidas">
						<i class="icon icon-in"></i>
						</a>
					</li>
					<li><a target="_blank" href="https://wa.me/5216622333816">
						<i class="fa fa-whatsapp"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="clearfix">
				<div class="logo-box">
					<div class="logo"> <a href="/">
							<img width="150" class="img-responsive" src="{{ asset('img/logo.png') }}" alt=""
								title="Business"></a>
					</div>
				</div>
				<div class="outer-box clearfix">
					<!-- Main Menu -->
					
					<nav class="main-menu logo-outer">
							
						<div class="navbar-header">
							<!-- Toggle Button -->
							<button type="button" class="navbar-toggle" data-toggle="collapse"
								data-target=".navbar-collapse"> <span class="icon-bar"></span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> </button>
						</div>
						<div class="navbar-collapse collapse clearfix">
							<ul class="navigation clearfix">
								<li> 
									<a class="nav-link current" href="{{ route('home') }}">Inicio</a>
								</li>
								<li> 
									<a class="nav-link" href="{{ route('products') }}">Productos</a>
								</li>
								<li> 
									<a class="nav-link" href="{{ route('contact') }}">Sucursales</a>
								</li>
								<li> 
									<a class="nav-link" href="{{ route('usa') }}">USA</a>
								</li>
							</ul>
						</div>
					</nav>
					<!-- Main Menu End-->
				</div>
			</div>
		</div>
	</div>
	<!--Header Main Box End-->
</header>
<!--End Main Header -->