<!--Section::Footer-->
  <footer class="main-footer">
      <div class="row clearfix main-footer-fix">
			<div class="row">
				<div class="col-md-4 fix-colum logos">
					<img  class="img-responsive" src="{{ asset('/img/colors/logo_aib.png') }}" alt="">
				</div>
				<div class="col-md-4 fix-colum logos">
					<img  class="img-responsive" src="{{ asset('/img/colors/logo_food.png') }}" alt="">
				</div>
				<div class="col-md-4 fix-colum logos">
					<img  class="img-responsive" src="{{ asset('/img/colors/logo_esr.png') }}" alt="">
				</div>
			</div>
        <div class="col-md-4 col-sm-6 col-xs-12">
			<div class="footer-1"> 
				<a href="{{route('home')}}">
				<img class="img-responsive" src="{{ asset('img/logo_footer.png') }}" alt="" title="Business">
				</a>
				<div class="col-md-12 co-sm-12">
					<ul class="footer-social fix-footer-social">
						<li>
							<a target="_blank" href="http://www.facebook.com/PortoAlegreBebidas">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li><a target="_blank" href="http://www.instagram.com/PortoAlegreBebidas_of">
							<i class="fa fa-instagram"></i>
							</a>
						</li>
						<li><a target="_blank" href="https://twitter.com/PortoAlegre_of">
							<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li><a target="_blank" href="https://www.linkedin.com/company/portoalegrebebidas">
							<i class="icon icon-in"></i>
							</a>
						</li>
						<li><a target="_blank" href="https://wa.me/5216622333816">
							<i class="fa fa-whatsapp"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
        </div>
        <!--Footer Column-->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="footer-2">
      <h4>MÉXICO</h4>
      <div class="separator"></div>
			<div class="row mt60">
				<span class="icon icon-phone"></span>
				<p class="mt20">(662) 108 18 50</p>
			</div>
			<div class="row mt60">
				<span class="icon icon-email"></span>
				<p class="mt20">atencion.clientes@portoalegre.com.mx</p>
			</div>
		</div>
        </div>
        <!--Footer Column-->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="footer-2">
      <h4>USA</h4>
      <div class="separator"></div>
			<div class="row mt60">
				<span class="icon icon-phone"></span>
				<p class="mt20">(602) 218 52 52</p>
			</div>
			<div class="row mt60">
				<span class="icon icon-email"></span>
				<p class="mt20">sales.usa@portoalegre.com.mx</p>
			</div>
          </div>
        </div>
        <!--Footer Column-->
      </div>
    <!--Copyright-->
    <div class="copyright">Copyright © 2019 - Todos los derechos reservados a PortoAlegre powered by <a target="_blank" href="https://innovathink.co/">Innovathink</a></div>
  </footer>
  <!--Section::Footer End -->