@extends('layouts.app')

@section('content')

@include('dynamic.slider_english')

<!--Section:: Testimonial-->
  <section class="testimonial style_2 mt60">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 about-text">
                        <h2 class="text-center title-about">OUR HISTORY AND WORK EXPERIENCE</h2>
                        <div class="text-center">
                            <p>Over 40 years’ experience in the industry of food & beverages enabled us to turn into the market and innovation leaders in Mexico’s food service arena.</p>
                            <br>
                            <p>• We have developed <span>strong R&D capabilities </span> to bring new products to the market.</p>
                            <br>
                            <p>• Solid team work structure to provide our clients with quality service.</p>
                            <br>
                            <p><span>Based on Northwestern México</span>, pretty close to the US-Mexico border, allowed us to reach a double-digit growth and to export 2.5 million gallons since <span>we entered the US market, back in 2006.</span> </p>
                            <br>
                            <p>Our <span>presence in Mexico covers more than 10 States</span> with direct sales and distribution operations, plus indirect sales through strategic alliances in the rest of the country.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  </section>
  <div class="divider mt60"></div>
    <!--Section:: Testimonial End -->
    <section class="testimonial mt60 ">
        <div class="section-content ">
            <div class="row">
              <div class="col-md-12">
                    <div  class="testimonial-item style_2 bg_usa" style="background-repeat: no-repeat; background-image: url({{asset('img/usa/yellow.png')}})">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title text-uppercase animated delay-2s rubberBand text-center">
                                    <h1>MAIN<br><span>VALUES</span></h1>
                                    <p class="disable-movil">Porto Alegre® offers a versatile product for a wide range of applications</p>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
          </div>
    </section>
    <section class="our_offer bg-light-grey">
        <div class="col-md-12">
            <div class="section-content">
                <div class="row">
                <div class="col-xs-5ths pn ">
                    <div class="item">
                    <div class="img-color-usa color-orange">
                        <div class="text-usa-i animated delay-2s zoomIn pb60">
                            <img src="{{ asset('img/usa/icon0.png') }}" alt="" class="img-response">
                            <p>UNIQUE REFRESHING <br> FLAVORS</p>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-xs-5ths pn">
                    <div class="item">
                        <div class="img-color-usa color-yellow">
                            <div class="text-usa-i animated delay-2s zoomIn">
                                <img src="{{ asset('img/usa/icon2.png') }}" alt="" class="img-response">
                                <p>ORIGINAL<br>RECIPES</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5ths pn">
                    <div class="item">
                        <div class="img-color-usa  color-orange">
                            <div class="text-usa-i animated delay-2s zoomIn">
                                <img src="{{ asset('img/usa/icon3.png') }}" alt="" class="img-response">
                                <p>GREAT BEVERAGE <br>EXPERIENCE</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5ths pn">
                    <div class="item">
                        <div class="img-color-usa  color-yellow">
                            <div class="text-usa-i animated delay-2s zoomIn">
                                <img src="{{ asset('img/usa/icon4.png') }}" alt="" class="img-response">
                                <p>NATURAL <br> INGREDIENTS</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5ths pn">
                    <div class="item">
                        <div class="img-color-usa  color-orange">
                            <div class="text-usa-i animated delay-2s zoomIn">
                                <img src="{{ asset('img/usa/icon5.png') }}" alt="" class="img-response">
                                <p>MADE WITH 100% <br>CANE SUGAR</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial ">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div  class="testimonial-item  bg_usa" style="background-repeat: no-repeat; background-image: url({{asset('img/usa/blue.jpg')}})">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title-2 text-uppercase animated delay-2s rubberBand text-center">
                                    <h1>OUR PRODUCTS<br><span>BEVERAGE SYRUP & AGUAS FRESCAS</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="portfolio">
        <div class="container-fluid">
          <div class="section-wrap">
              <div class="row clearfix">
                <div  class="col-md-3 fix-padding disable-movil w-30" >
                    <article class="post item">
                        <div class="entry-thumbnail">
                            <img id="imageInfo" class="img-responsive" src="{{ asset ('img/usa/infopremium.jpg') }}" alt="">
                            <div class="post-meta">
                                <div class="big-title  animated delay-2s rubberBand">
                                    <div class="divider-2"></div>
                                    <p id="textInfo" class="jsregular-22">Concentrate made with 100% natural ingredients. Our customers may enjoy an option that may enhance their experience of tasting authentic refreshing flavored water with great consistency, aroma and flavor.We also want to satisfy a variety of needs by using the same product. Both our expertise, and performance make Porto Alegre Premium the ideal mixer for exclusive drinks and offers essential support to chefs and mixologists.</p>
                                    <br>
                                    <div class="divider-2"></div>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="portfolio col-md-9 gutter-less mtn w-70" >
                    <div class="row">
                        <div class="portfolio-item default-gallery-item style_2 item-one col-lg-12 active-movil">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img id="logoMovil" src="{{ asset ('/img/usa/premium_0.jpg') }}"  class="img-responsive">
                                    <a href="javascript:;" class="lightbox-image image-link"><span class="icofont icofont-plus-circle"></span></a>
                                
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                        <div data-category="premium" class="portfolio-item default-gallery-item style_2 item-one col-lg-12 categoryContent" style="display:none;">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box category" data-category="premium" >
                                    <img src="{{ asset ('img/usa/premium.jpg') }}"  class="img-responsive">
                                    <a href="javascript:;" class="lightbox-image image-link"><span class="icofont icofont-plus-circle"></span></a>
                                
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                        <div data-category="readydrink" class="portfolio-item default-gallery-item style_2 item-one item-three col-lg-12 categoryContent" >
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <figure class="image-box category" data-category="readydrink">
                                <img src="{{ asset ('img/usa/readyDrink.jpg') }}" alt="" class="img-responsive">
                                <a href="javascript:;" class="lightbox-image image-link" ><span class="icofont icofont-plus-circle"></span></a>
                            </figure>
                            <!--Overlay Box-->
                        </div>                          
                        </div>
                    </div>
                    <div class="row">
                        <div  class="portfolio-item default-gallery-item style_2 item-one col-md-12">
                            <div class="inner-box"> 
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img class="img-flavor animated" src="{{ asset ('/img/usa/products/premium/premium0.jpg') }}" alt="" class="img-responsive" >
                                </figure>
                                <!--Overlay Box-->
                            </div>                          
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 fix-padding">
                            <div class="premium-slider flavors" id="premium" > 
                                <!-- Logo item -->
                                <div data-flavor="premium0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor0.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor1.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor2.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium3.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor3.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium4.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor4.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium5.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor8.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium7.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor9.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium6.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor6.jpg')}}"></div>
                                <!-- Logo item -->
                                <div data-flavor="premium8.jpg" class="item flavor"><img alt="" src="{{asset('img/products/premium/flavors/flavor10.jpg')}}"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row " >
                        <div class="col-md-12 fix-padding">
                        <div style="display:none;" class="readyDrink-slider flavors" id="readydrink"> 
                            <!-- Logo item -->
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="readydrink0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                            <div data-flavor="readydrink0.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor0.jpg')}}"></div>
                            <div data-flavor="readydrink2.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor2.jpg')}}"></div>
                            <div data-flavor="readydrink1.jpg" class="item flavor"><img alt="" src="{{asset('img/products/readydrink/flavors/flavor1.jpg')}}"></div>
                        </div>
                        </div>
                    </div>
                </div>
                  
              </div>
          </div>
       </div>
    </section>
    
    
    <section class="portfolio">
        <div class="container-fluid">
            <div class="section-wrap">
                <div class="row clearfix">
                <div class="portfolio col-3 gutter-less mtn">
                    <div class="portfolio-item default-gallery-item style_2 item-one">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner0.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                    <div class="portfolio-item default-gallery-item style_2 item-two">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner1.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                    <div class="portfolio-item default-gallery-item style_2 item-one item-three">
                        <div class="inner-box"> 
                        <!--Image Box-->
                        <figure class="image-box">
                            <img src="{{asset('img\products\banner2.png')}}" alt="" class="img-responsive">
                        </figure>
                        <!--Overlay Box-->
                        </div>                          
                    </div>
                </div>
                <!-- <div class="text-center"><a href="#" class="btn theme-btn mt40">View More </a></div> -->
                </div>
            </div>
        </div>
    </section> 
    <section class="testimonial active-movil">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div  class="testimonial-item  bg_usa" style="background-repeat: no-repeat; background-image: url({{asset('img/colors/banner_red.jpg')}})">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title text-uppercase animated delay-2s rubberBand text-center">
                                    <h1>DIVERSITY OF <br><span>APPLICATIONS</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="col-prescribe">
                    <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <a href="{{ asset('/pdfs/aguas_frescas_usa.pdf') }}" download>
                                <figure class="image-box " >
                                    <img src="{{ asset('/img/colors/waters.jpg') }}" class="img-responsive">
                                    <div class="lightbox-content-usa">
                                        <h1 class="fix-title-usa">REFRESHING FLAVORED WATERS<br><span >CUCUMBER LEMONADE</span></h1>
                                    </div>
                                    <div class="pdf-content  animated infinite delay-6s bounce">
                                        <img class="pdf-download" src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                    </div>
                                </figure>
                            </a>
                            <!--Overlay Box-->
                        </div>                          
                    </div>

                    <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <a href="{{ asset('/pdfs/cocteleria_usa.pdf') }}" download>
                                <figure class="image-box " >
                                    <img src="{{ asset('/img/colors/seafood.jpg') }}" class="img-responsive">
                                    <div class="lightbox-content-usa">
                                        <h1 class="fix-title-usa">COCKTAILS<br><span >TAMARIND MARGARITA</span></h1>
                                    </div>
                                    <div class="pdf-content  animated infinite delay-6s bounce">
                                        <img class="pdf-download" src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                    </div>
                                </figure>
                            </a>
                            <!--Overlay Box-->
                        </div>                          
                    </div>
                    <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <a href="{{ asset('/pdfs/aderezos_usa.pdf') }}" download>
                                <figure class="image-box " >
                                    <img src="{{ asset('/img/colors/dressings.jpg') }}" class="img-responsive">
                                    <div class="lightbox-content-usa">
                                        <h1 class="fix-title-usa">DRESSINGS<br><span >HOT WINGS DRESSING</span></h1>
                                    </div>
                                    <div class="pdf-content  animated infinite delay-6s bounce">
                                        <img class="pdf-download" src="{{ asset('/img/pdf.png') }}" alt="" srcset="">
                                    </div>
                                </figure>
                            </a>
                            <!--Overlay Box-->
                        </div>                          
                    </div>
                </div>
                <div class="col-prescribe-banner disable-movil">
                    <div class="portfolio-item default-gallery-item style_2 item-one" style="max-height: 100%">
                        <div class="inner-box"> 
                            <!--Image Box-->
                            <figure class="image-box " >
                                <img src="{{ asset('/img/colors/banner_red.jpg') }}" class="img-responsive">
                                <div class="lightbox-content-usa-2">
                                    <h1 >DIVERSITY OF <br><span>APPLICATIONS</span></h1>
                                </div>
                            
                            </figure>
                            <!--Overlay Box-->
                        </div>                          
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial ">
        <div class="section-content">
            <div class="row">
                <div class="col-md-12">
                    <div  class="testimonial-item  bg_usa" style="background-repeat: no-repeat; background-image: url({{asset('img/contact/banner_purpple.jpg')}})">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="usa-title text-uppercase animated delay-2s rubberBand text-center">
                                    <h1>BEING IN TOUCH WITH YOU<br><span >IS OUR PRIORITY</span></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>
    <div class="disable-movil" style="    top: 0px;
    position: relative;">
        <div class="map-title text-uppercase animated delay-1s flipInY">
            <h1>WE ARE HERE<br><span>FOR YOU</span></h1>
            <div class="map-subtitle">
                <img src="{{ asset('/img/contact/marker.png') }}" alt="" srcset="">
                <h2>PORTO ALEGRE<span>DISTRIBUTION CENTER</span></h2>
            </div>
            <div class="map-subtitle">
                <span class="circle"></span>
                <h2>PRESENCE<span> IN MÉXICO</span></h2>
            </div>
            <div class="clear-fix"></div>
            <div class="map-subtitle" style="margin-top: 33px;">
                <span class="circle green"></span>
                <h2>PRESENCE<span> IN THE US</span></h2>
            </div>
        </div>
        @include('dynamic.map')

    </div>
    <section>
        <div class="row">
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_aib.jpg') }}" alt="">
            </div>
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_food.jpg') }}" alt="">
            </div>
            <div class="col-md-4 fix-colum logos">
                <img  class="img-responsive" src="{{ asset('/img/colors/logo_esr.jpg') }}" alt="">
            </div>
        </div>
    </section>

@endsection


@section('scripts')

<script>
    var selected='premium';
    var currentImg = '';
    var text = {
        premium : 'Beverage Syrup made with 100% natural ingredients that allow us to offer our customers an option with a consistency, aroma and flavor that enhances the experience of authentic fresh waters.\nWe also seek to satisfy a variety of uses with the same product; Our profiles and performance make Porto Alegre Premium ideal for exclusive drinks and essential support for chefs and mixologists',
        readyDrink :'Aguas Frescas made with 100% natural ingredients that allow us to offer our customers an option with a consistency, aroma and flavor that enhances the experience of authentic Aguas Frescas.'
    }
    
    {{-- $('.flavor').click(function(){
        $('.img-flavor').attr('src','/img/usa/products/'+selected+'/'+$(this).attr('data-flavor'));
    }); --}}

    $('.flavor').click(function(){
        $('.img-flavor').removeClass('fadeInUp');
        $('.img-flavor').addClass('fadeOutUp');
        currentImg = $(this).attr('data-flavor');
        setTimeout(function() {   //calls click event after a certain time
            $('.img-flavor').attr('src','/img/usa/products/'+selected+'/'+currentImg);
            $('.img-flavor').removeClass('fadeOutUp');
            $('.img-flavor').addClass('fadeInUp');
         }, 300);
    });

    $('.category').click(function(){
        selected = $(this).attr('data-category');
        $('.img-flavor').attr('src','/img/usa/products/'+selected+'/'+selected+'0.jpg');

        $('.flavors').css('display','none');
        $('#'+$(this).attr('data-category')).css('display','block');
        $('#textInfo').text(text[selected]);
        $('#imageInfo').attr('src','/img/usa/'+'info'+selected+'.jpg');
        $('#logoMovil').attr('src','/img/usa/'+selected+'_0.jpg');

        $.each($('.categoryContent'), function( key, item ) {
            if($(item).attr('data-category') == selected){
                $(item).css('display','none');
            }else{
                $(item).css('display','block');
            }
        });
    });
   

</script>

@endsection